#!/bin/sh

make clean -C chess
cd pbbs
make clean -C breadthFirstSearch/lockingBFS
make clean -C delaunayRefine/lockingRefine
make clean -C dictionary/lockingHash
make clean -C maximalIndependentSet/lockingMIS
make clean -C maximalMatching/lockingMatching
make clean -C spanningForest/lockingST
cd -
