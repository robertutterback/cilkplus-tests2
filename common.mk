# -*- mode: makefile-gmake; -*-

# Create your own myconfig.mk in the top directory (i.e., same as this file)
# to include COMILER, COMPILER_HOME, and CILKRTS_HOME
TEST_MK_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
include $(TEST_MK_DIR)/myconfig.mk

INCLUDE=$(CILKRTS_HOME)/include
# ADDED_FLAGS = -fno-inline-detach
# LTO = -flto
# OPT = -O3 -march=native -DNDEBUG $(LTO)
# BASIC_CFLAGS = -g -W -Wall -fcilkplus $(ADDED_FLAGS) -Werror -I$(INCLUDE) $(OPT)
# BASIC_CXXFLAGS=$(BASIC_CFLAGS)

CFLAGS = $(BASIC_CFLAGS) -W -Wall -Werror
CXXFLAGS = $(BASIC_CXXFLAGS) -W -Wall -Werror

# CILKRTS_DLIB=-Wl,-rpath -Wl,$(CILKRTS_HOME)/lib
CILKRTS_SLIB=$(CILKRTS_HOME)/lib/libcilkrts.a

ifdef PORR_HOME
	CFLAGS += -DPORR=1
	CXXFLAGS += -DPORR=1
	export CPATH+=$(PORR_HOME)/src
	PORR_SLIB=$(PORR_HOME)/build/libporr.a
	LDLIBS = $(PORR_SLIB)
endif

# LDFLAGS = -L$(CILKRTS_HOME)/lib $(CILKRTS_DLIB)
# LDLIBS= -lrt -ldl -lpthread -lcilkrts
LDFLAGS += $(CILKRTS_SLIB)
LDLIBS += $(LIBS)
ifeq ($(LTO),1)
	ARFLAGS=--plugin $(COMPILER_HOME)/lib/LLVMgold.so
else
	ARFLAGS=
endif

ifeq ($(COMPILER),LLVM)
        CC = $(COMPILER_HOME)/bin/clang
        CXX = $(COMPILER_HOME)/bin/clang++
else ifeq ($(COMPILER), GCC)
        CC = $(COMPILER_HOME)/bin/gcc
        CXX = $(COMPILER_HOME)/bin/g++
endif

.PHONY : default clean

default : $(TARGETS)

# Each C source file will have a corresponding file of prerequisites.
# Include the prerequisites for each of our C source files.
-include $(OBJ:.o=.d)

# This rule generates a file of prerequisites (i.e., a makefile)
# called name.d from a C source file name.c.
%.d : CFLAGS += -MM -MP
%.d : %.c
	@set -e; rm -f $@; \
	$(CC) $(CFLAGS) -MF $@.$$$$ $<; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

# This rule generates a file of prerequisites (i.e., a makefile)
# called name.d from a CPP source file name.cpp.
%.d : CXXFLAGS += -MM -MP
%.d : %.cpp
	@set -e; rm -f $@; \
	$(CXX) $(CXXFLAGS) -MF $@.$$$$ $<; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

%.o : %.c
	$(CC) $(CFLAGS) -c $<

%.o : %.cpp
	$(CXX) $(CXXFLAGS) -c $<

%.o : %.cc
	$(CXX) $(CXXFLAGS) -c $<
