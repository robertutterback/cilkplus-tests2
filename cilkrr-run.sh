#!/bin/bash
set -e
ulimit -v 6291456
MAXTIME="5m"
CORES="1 2" # 4 6 8"
NITER=1

bench=(matching) # MIS BFS) #refine dict MIS matching ST BFS)
declare -A dirs args

dirs["chess"]=chess
args["chess"]=""

dirs["dict"]=pbbs/dictionary/lockingHash
args["dict"]="-r 1 ../sequenceData/data/randomSeq_1000000_1000_int "

dirs["MIS"]=pbbs/maximalIndependentSet/lockingMIS
args["MIS"]="-r 1 ../graphData/data/randLocalGraph_J_5_100000"

dirs["matching"]=pbbs/maximalMatching/lockingMatching
args["matching"]="-r 1 ../graphData/data/randLocalGraph_E_5_100000"

dirs["ST"]=pbbs/spanningForest/lockingST
args["ST"]="-r 1 ../graphData/data/randLocalGraph_E_5_10000"

dirs["BFS"]=pbbs/breadthFirstSearch/lockingBFS
args["BFS"]="-r 1 ../graphData/data/randLocalGraph_J_5_100000"

dirs["refine"]=pbbs/delaunayRefine/lockingRefine
# Trying refine on smaller inputs tells me that it runs out of vertices...
args["refine"]="-r 1 ../geometryData/data/2DinCubeDelaunay_100000"


errcheck () {
    errcode=$1
    logname=$2
    # printf "\t"
    # I'm not sure tail -1 is right for delaunay refinement
    if [[ $errcode -eq 0 ]]; then
        printf "%0.2f" "$(grep 'time' log | tail -1 | cut -d':' -f 2 | tr -d ' ')"
    else
        printf "\n[Error]:\n"
				if [[ $errcode -eq 124 ]]; then
						printf "Timed out after %s.\n" $MAXTIME
						exit 1
				fi
        cat log
        exit 1
    fi
}

runcmd() {
		local P=$1
		local mode=$2
		local name="${3}_porr_spin"
		local name="${3}_porr_try"
		local args=$4
		CILK_NWORKERS=$P CILKRR_MODE=$mode timeout $MAXTIME ./$name $args &> log
}

runreplay() {
		name=$1
		args=$2

		for P in $CORES; do
				avg=0
				for i in $(seq $NITER); do
						runcmd "$P" "replay" "$name" "$args"
						val=$(errcheck $? "log")
						avg=$( echo "scale=2; $avg + $val" | bc )
				done
				avg=$( echo "scale=2; $avg / $NITER" | bc )
				printf "\t%s" "$avg"
    done

}

runall () {
    set +e

		## Hack
		if [[ "$1" = "chess" ]]; then
				name="chess-cover-locking"
		else
				name=$1
		fi
    args=$2

    printf -- "--- $name $args ---\n"
		header_left="P\tbase\trecord"
		printf "${header_left}\t\t\treplayP\n"
		printf "%${#header_left}s\t" | tr " " "=" | tr "\t" "====="
		for P in $CORES; do printf "\t$P"; done;
		printf "\n"

		## Currently, I just run the base several times, then record
		## several times, then each replayP several tims. This is not
		## really the right way to do things. For every record iteration,
		## I ought to run several replay iterations, so that the replay
		## get tested on different records. I should switch to python to
		## do this.

    for P in $CORES; do
				printf "$P"
				avg=0
				for i in $(seq $NITER); do
						runcmd "$P" "none" "$name" "$args"
						val=$(errcheck $? "log")
						avg=$( echo "scale=2; $avg + $val" | bc )
				done
				avg=$( echo "scale=2; $avg / $NITER" | bc )
				printf "\t%s" "$avg"

				avg=0
				for i in $(seq $NITER); do
						runcmd "$P" "record" "$name" "$args"
						val=$(errcheck $? "log")
						avg=$( echo "scale=2; $avg + $val" | bc )
				done
				avg=$( echo "scale=2; $avg / $NITER" | bc )
				printf "\t%s" "$avg"

				runreplay "$name" "$args"
				printf "\n"
		done
    printf "\n-------------------------"
    set -e
}

if [ $# -gt 0 ]; then
    bench=($1)
fi

for b in "${bench[@]}"; do
    #./build.sh "$b"
    cd ${dirs[$b]}
    runall "$b" "${args[$b]}"
    cd -
done
