A set of Cilk Plus benchmarks.

To compile and run, add a "myconfig.mk" Makefile at the top directory.
In that file, initialize the following variables:

COMPILER ?= <LLVM or GCC>
COMPILER_HOME ?= <the dir that you install your compiler which contains bin/clang>
CILKRTS_HOME ?= <the dir that you install your Cilk Plus runtime which contains lib/libcilkrts.so>
CILKRR_HOME ?= <the source directory for cilkrr>

To compile the basic benchmarks:

	cd basic
	make

To compile the pbbs benchmarks: see the QUICK START GUIDE in top-level
README (you need to set some environment variables and modify
common/parallelDefs)

The script "build.sh" will also compile the pbbs benchmarks and the
chess benchmark, although it is really meant to be used with the
PORRidge project.
