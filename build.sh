#!/bin/bash
set -e

chess() {
    cd chess
    make clean; make CILK=1
    mv chess-cover-locking chess-cover-locking_base_spin
    make clean; make CILK=1 PORR=1
		mv chess-cover-locking chess-cover-locking_porr_spin
    cd -
}

BFS() {
    pushd pbbs/breadthFirstSearch
    cd common; make clean; make CILK=1; cd -
    cd lockingBFS
    make clean; make CILK=1
    mv BFS BFS_base_spin
    make clean;  make CILK=1 PORR=1
		mv BFS BFS_porr_spin
		make clean; make CILK=1 TRYLOCK=1
		mv BFS BFS_base_try
		make clean; make CILK=1 TRYLOCK=1 PORR=1
		mv BFS BFS_porr_try
    popd
}
refine() {
    pushd pbbs/delaunayRefine
    cd common; make clean; make CILK=1; cd -
    cd lockingRefine
    make clean; make CILK=1
    mv refine refine_base_spin
    make clean; make CILK=1 PORR=1
		mv refine refine_porr_spin
    popd
}
dict() {
    pushd pbbs/dictionary
    cd common; make clean; make CILK=1; cd -
    cd lockingHash
    make clean; make CILK=1
    mv dict dict_base_spin
    make clean; make CILK=1 PORR=1
		mv dict dict_porr_spin
    popd
}
MIS() {
    pushd pbbs/maximalIndependentSet
    cd common; make clean; make CILK=1; cd -
    cd lockingMIS
    make clean; make CILK=1
    mv MIS MIS_base_spin
    make clean; make CILK=1 PORR=1
		mv MIS MIS_porr_spin
		make clean; make CILK=1 TRYLOCK=1
    mv MIS MIS_base_try
    make clean; make CILK=1 PORR=1 TRYLOCK=1
		mv MIS MIS_porr_try

    popd
}
matching() {
    pushd pbbs/maximalMatching
    cd common; make clean; make CILK=1; cd -
    cd lockingMatching
    make clean; make CILK=1
    mv matching matching_base_spin
    make clean; make CILK=1 PORR=1
		mv matching matching_porr_spin
		make clean; make CILK=1 TRYLOCK=1
    mv matching matching_base_try
    make clean; make CILK=1 PORR=1 TRYLOCK=1
		mv matching matching_porr_try
    popd
}

pushd $(dirname $0)

if [ $# -gt 0 ]; then
    eval $1
else
   #chess
   BFS
   #refine
   #dict
   MIS
   matching
fi

popd
