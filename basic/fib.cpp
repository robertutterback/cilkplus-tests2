/*
 * Copyright (c) 1994-2003 Massachusetts Institute of Technology
 * Copyright (c) 2003 Bradley C. Kuszmaul
 * Copyright (c) 2013 I-Ting Angelina Lee and Tao B. Schardl 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include <stdlib.h>
#include <stdio.h>

#include <sched.h>

#include "ktiming.h"
 
int fib(int n) {
    if (n < 2) { 
        return (n);

    } else {
        int x = 0;
        int y = 0;

        x = cilk_spawn fib(n - 1);
        y = fib(n - 2);
        cilk_sync;

        return (x + y);
    }
}

int main(int argc, char *argv[]) {

    int n, result;
    int start = 0;
    int p = 0;
    clockmark_t begin, end;

    if (argc < 2) {
        fprintf(stderr, "Usage: fib [<cilk options>] <n> [start] [nproc], where\n");
        fprintf(stderr, "\t <n> is the input for fib,\n");
        fprintf(stderr, "\t <start> specifies first CPU in the range to use, and\n");
        fprintf(stderr, "\t <nproc> specifies the number of CPUs to use.\n");
        exit(1); 
    }

    n = atoi(argv[1]);

    if(argc == 4) {
        start = atoi(argv[2]);
        p = atoi(argv[3]);

        cpu_set_t set_used; 
        CPU_ZERO(&set_used);
        for(int i=start; i < (start+p); i++) {
            CPU_SET(i, &set_used);
        }
        sched_setaffinity(0, sizeof(cpu_set_t), &set_used);
        printf("Running fib %d with procs [%d--%d].\n", n, start, (start+p));
    }

    begin = ktiming_getmark();
    result = fib(n);
    end = ktiming_getmark();
    double elapsed_time = ktiming_diff_sec(&begin, &end);

    printf("Result: %d\n", result);
    printf("Elapsed time in second: %f\n", elapsed_time);

    __cilkrts_end_cilk();

    return 0;
}
