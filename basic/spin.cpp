#include <stdio.h>
#include <cilk/cilk.h>

void spin(volatile int *x, int spin_until) {
    int a[100];
    for(int i=0; i < 100; i++) {
        a[i] = spin_until;
    }
    fprintf(stderr, "spin called with spinuntil = %d.\n", spin_until);
    while(*x < spin_until);
    fprintf(stderr, "spin called with spinuntil complete = %d.\n", spin_until);
}

void do_nothing() {
    for(int i=0; i < 100000; i++) {
         i *= 1;
    }
    return;
}

void do_stuff(void) {

    volatile int x = 0;
    do_nothing();
    fprintf(stderr, "spawn spin called with spinuntil = %d.\n", x + 1);
    cilk_spawn spin(&x, 1);
    x++;
    fprintf(stderr, "spawn spin called with spinuntil = %d.\n", x + 1);
    cilk_spawn spin(&x, 2);
    x++;
    fprintf(stderr, "spawn spin called with spinuntil = %d.\n", x + 1);
    cilk_spawn spin(&x, 3);
    x++;
    fprintf(stderr, "spawn spin called with spinuntil = %d.\n", x + 1);
    cilk_spawn spin(&x, 4);
    x++;
    
    /* cilk_sync;

    cilk_spawn spin(&x, 5);
    x++;
    cilk_spawn spin(&x, 6);
    x++;
    cilk_spawn spin(&x, 7);
    x++;
    cilk_spawn spin(&x, 8);
    x++; */ 

    cilk_sync;
}

void place_holder() {
    do_stuff();
    return;
}

int main(void) {
    cilk_spawn place_holder();
    cilk_sync;

    return 0;
}

