static const char *ident __attribute__((__unused__))
     = "$HeadURL: https://bradley.csail.mit.edu/svn/repos/cilk/5.4.3/examples/fib.cilk $ $LastChangedBy: sukhaj $ $Rev: 517 $ $Date: 2003-10-27 10:05:37 -0500 (Mon, 27 Oct 2003) $";

/*
 * Copyright (c) 1994-2003 Massachusetts Institute of Technology
 * Copyright (c) 2003 Bradley C. Kuszmaul
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <cilk/cilk.h>
#include <cilktime.h> 
#include <iostream>


#define CILK_FOR_GRAINSIZE 128

double dowork(double i) {
    // Waste time:
    int j;
    double k = i;
    for (j = 0; j < 50000; ++j) {
        k += k / ((j + 1) * (k + 1));
    }

    return k;
}

extern "C"
int cilk_main(int argc, char *argv[]) {

    unsigned int n = 10000;
    unsigned long long tm_begin; 
    double tm_elapsed;

    if (argc > 1) {
        n = strtoul(argv[1], 0, 0);
    }

    double* a = new double[n];

    cilk_for(unsigned int i = 0; i < n; i++) {
        // Populate A 
        a[i] = (double) ((i * i) % 1024 + 512) / 512;
    }

    std::cout << "Iterating over " << n << " integers" << std::endl;

    tm_begin = cilk_getticks(); 
#pragma cilk grainsize=CILK_FOR_GRAINSIZE
    cilk_for(unsigned int i = 0; i < n; ++i) {
        a[i] = dowork(a[i]);
    }
    tm_elapsed = cilk_ticks_to_seconds(cilk_getticks() - tm_begin);

    std::cout << "Parallel loop took " << tm_elapsed << " seconds" << std::endl;

    return 0;
}

