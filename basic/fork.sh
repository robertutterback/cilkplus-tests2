#!/bin/sh

set -m # Enable Job Control

# for i in `seq 5`; do # start 30 jobs in parallel
#   CILK_NWORKERS=8 ./fib 42 &
# done

CILK_NWORKERS=1 taskset -c 0 ./fib 42 &
CILK_NWORKERS=1 taskset -c 8 ./fib 42 &
CILK_NWORKERS=1 taskset -c 16 ./fib 42 &
CILK_NWORKERS=1 taskset -c 24 ./fib 42 &

# Wait for all parallel jobs to finish
while [ 1 ]; do fg 2> /dev/null; [ $? == 1 ] && break; done
