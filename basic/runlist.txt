./fib 42
./fibx 280
./cholesky -c -n 4000 -z 40000
./cilksort -c -n 100000000
./fft -benchmark long
./heat -nx 2048 -ny 2048 -nt 500
./knapsack -benchmark long
./lu -n 4096
./matmul -n 2048 -rc
./nqueens 14
./rectmul -benchmark long -c
./strassen -benchmark long -rc
