// This code is part of the Problem Based Benchmark Suite (PBBS)
// Copyright (c) 2011 Guy Blelloch and the PBBS team
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights (to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <iostream>
#include "sequence.h"
#include "graph.h"
#include "parallel.h"
using namespace std;

//Flags[v] = 0 if v is not in nor out of MIS
//Flags[v] = 1 if v is in MIS
//Flags[v] = 2 if v is out of in MIS (one of its neighbors is in)
#define UNDECIDED  ((char)0)
#define IN_MIS     ((char)1)
#define OUT_OF_MIS ((char)2)

// **************************************************************
//    MAXIMAL INDEPENDENT SET
// **************************************************************

#ifdef TRYLOCK
void maxIndSetNonDeterministic(intT n, vertex<intT>* G, char* Flags) {
  intT* V = newA(intT,n);
  lock_t* L = newA(lock_t,n);
  utils::reserve_locks(n);

  parallel_for(intT i=0; i<n; i++) {
    V[i] = INT_T_MAX; 
    utils::lock_init(&L[i], i);
  }

  //fprintf(stderr, "Begin: %s\n", porr::get_pedigree().c_str());

#pragma cilk grainsize = 1024
  parallel_for(intT i=0; i<n; i++) {
    intT v = i;
    while(1) {
      // fprintf(stderr, "i(%i, %i): %s\n", i, iter,
      //         porr::get_pedigree().c_str());

      //drop out if already in or out of MIS
      if(Flags[v] != UNDECIDED) break;

      //try to lock self and neighbors
      //utils::CAS(&V[v], INT_T_MAX, (intT)0)
      if(V[v] == INT_T_MAX && utils::try_lock(&L[v])) {
        if(V[v] == INT_T_MAX) {
          V[v] = (intT)0;
          utils::unlock(&L[v]);

          intT k = 0;
          for(intT j=0; j<G[v].degree; j++) {
            intT ngh = G[v].Neighbors[j];
            //if ngh is not in MIS or we successfully 
            //acquire lock, increment k
            //if(Flags[ngh] || utils::CAS(&V[ngh], INT_T_MAX, (intT)0)) k++;
            if(Flags[ngh] == OUT_OF_MIS) {
              k++;
            } else {
              //utils::CAS(&V[ngh], INT_T_MAX, (intT)0))
              if(V[ngh] == INT_T_MAX && utils::try_lock(&L[ngh])) {
                if(V[ngh] == INT_T_MAX) {
                  V[ngh] = (intT)0;
                  k++;
                  utils::unlock(&L[ngh]);
                } else {
                  utils::unlock(&L[ngh]);
                  break;
                }
              } else {
                break;
              }
            }
          }

          if(k == G[v].degree) {
            //win on self and neighbors so fill flags
            Flags[v] = IN_MIS;
            for(intT j=0;j<G[v].degree;j++) {
              intT ngh = G[v].Neighbors[j]; 
              if(Flags[ngh] != OUT_OF_MIS) { Flags[ngh] = OUT_OF_MIS; }
            }
          } else {
            //lose so reset V values up to point where it lost
            V[v] = INT_T_MAX;
            for(intT j=0; j<k; j++) {
              intT ngh = G[v].Neighbors[j];
              if(Flags[ngh] != OUT_OF_MIS) { V[ngh] = INT_T_MAX; }
            }
          }
        } else {
          utils::unlock(&L[v]);
        }
      }
    }
  }

  parallel_for(intT i=0; i<n; i++) {
    utils::lock_destroy(&L[i]);
  }

  free(V);
  free((void*)L);
}

#else

void maxIndSetNonDeterministic(intT n, vertex<intT>* G, char* Flags) {
  intT* V = newA(intT,n);

#ifdef LOCAL_LOCKS
#define GETLOCK(i) &G[i].lock
#else
  lock_t* L = newA(lock_t,n);
#define GETLOCK(i) &L[i]
#endif
  utils::reserve_locks(n);

  parallel_for(intT i=0; i<n; i++) {
    V[i] = INT_T_MAX;
    utils::lock_init(GETLOCK(i), i);
  }


#pragma cilk grainsize = 1024
  parallel_for(intT i=0; i<n; i++) {
    intT v = i;

    while(1) {
      //drop out if already in or out of MIS
      //if(Flags[v] != UNDECIDED) break;

      //try to lock self and neighbors
      //utils::CAS(&V[v], INT_T_MAX, (intT)0)
      utils::lock(GETLOCK(v));
      if(Flags[v] != UNDECIDED) {
        utils::unlock(GETLOCK(v));
        break;
      }
      if(V[v] == INT_T_MAX) {
        V[v] = (intT)0;
        utils::unlock(GETLOCK(v));

        intT k = 0;
        for(intT j=0; j<G[v].degree; j++) {
          intT ngh = G[v].Neighbors[j];
          utils::lock(GETLOCK(ngh));
          if (Flags[ngh] == OUT_OF_MIS) k++;
          else if (V[ngh] == INT_T_MAX) {
            V[ngh] = (intT)0;
            k++;
          }
          // else
          //  done = 1;
          utils::unlock(GETLOCK(ngh));
          // if (done) break;
        }

        if(k == G[v].degree) {
          //win on self and neighbors so fill flags
          utils::lock(GETLOCK(v));
          Flags[v] = IN_MIS;
          utils::unlock(GETLOCK(v));
          for(intT j=0;j<G[v].degree;j++) {
            intT ngh = G[v].Neighbors[j]; 
            utils::lock(GETLOCK(ngh));
            if(Flags[ngh] != OUT_OF_MIS) { Flags[ngh] = OUT_OF_MIS; }
            utils::unlock(GETLOCK(ngh));
          }
        } else {
          //lose so reset V values up to point where it lost
          utils::lock(GETLOCK(v));
          V[v] = INT_T_MAX;
          utils::unlock(GETLOCK(v));
          //for(intT j=0; j<k; j++) {
          for(intT j=0; j<G[v].degree; j++) {
            intT ngh = G[v].Neighbors[j];
            utils::lock(GETLOCK(ngh));
            if(Flags[ngh] != OUT_OF_MIS) { V[ngh] = INT_T_MAX; }
            utils::unlock(GETLOCK(ngh));
          }
        }
        
        // for(intT j=0;j<G[v].degree;j++) {
        //  intT ngh = G[v].Neighbors[j]; 
        //  utils::unlock(&L[ngh]);
        // }

      } else {
        utils::unlock(GETLOCK(v));
      }
    }
  }

#ifndef USE_LOCKSTAT
  parallel_for(intT i=0; i<n; i++) {
    utils::lock_destroy(GETLOCK(i));
  }
  free((void*)L);
#endif
  
  free(V);
}
#endif // TRYLOCK

void brokenCompiler(char* Flags, intT n) {
  parallel_for (intT i=0; i < n; i++) Flags[i] = 0;
}

char* maximalIndependentSet(graph<intT> G) {
  intT n = G.n;
  char* Flags = newA(char,n);
  brokenCompiler(Flags, n);
  maxIndSetNonDeterministic(n, G.V, Flags);
  return Flags;
}
