#!/bin/sh
# Script from Julian Shun
# commented out benchmarks don't exist 

make -s -j -C removeDuplicates/common
make -s -j -C removeDuplicates/deterministicHash
make -s -j -C removeDuplicates/serialHash

make -s -j -C dictionary/common
make -s -j -C dictionary/deterministicHash
make -s -j -C dictionary/serialHash

make -s -j -C integerSort/common
make -s -j -C integerSort/blockRadixSort
make -s -j -C integerSort/serialRadixSort

make -s -j -C nearestNeighbors/common
# make -s -j -C nearestNeighbors/octTreeNeighbors
make -s -j -C nearestNeighbors/octTree2Neighbors

make -s -j -C rayCast/common
make -s -j -C rayCast/kdTree

make -s -j -C nBody/common
# make -s -j -C nBody/parallelBarnesHut
make -s -j -C nBody/parallelCK

make -s -j -C suffixArray/common
make -s -j -C suffixArray/parallelKS
# make -s -j -C suffixArray/parallelRange
make -s -j -C suffixArray/serialKS

make -s -j -C delaunayTriangulation/common
make -s -j -C delaunayTriangulation/incrementalDelaunay
make -s -j -C delaunayTriangulation/serialDelaunay

make -s -j -C delaunayRefine/common
make -s -j -C delaunayRefine/incrementalRefine

make -s -j -C convexHull/common
make -s -j -C convexHull/quickHull
make -s -j -C convexHull/serialHull

make -s -j -C comparisonSort/common
make -s -j -C comparisonSort/serialSort
make -s -j -C comparisonSort/stlParallelSort
make -s -j -C comparisonSort/sampleSort
# make -s -j -C comparisonSort/quickSort

make -s -j -C breadthFirstSearch/common
make -s -j -C breadthFirstSearch/ndBFS
make -s -j -C breadthFirstSearch/serialBFS
make -s -j -C breadthFirstSearch/deterministicBFS

make -s -j -C maximalMatching/common
make -s -j -C maximalMatching/ndMatching
make -s -j -C maximalMatching/serialMatching
make -s -j -C maximalMatching/incrementalMatching

make -s -j -C maximalIndependentSet/common
make -s -j -C maximalIndependentSet/incrementalMIS
make -s -j -C maximalIndependentSet/ndMIS
make -s -j -C maximalIndependentSet/serialMIS
# make -s -j -C maximalIndependentSet/luby

make -s -j -C minSpanningForest/common
make -s -j -C minSpanningForest/parallelKruskal
make -s -j -C minSpanningForest/serialMST

make -s -j -C spanningForest/common
make -s -j -C spanningForest/incrementalST
make -s -j -C spanningForest/serialST
# make -s -j -C spanningForest/newST
make -s -j -C spanningForest/ndST

# make -s -j -C lassoRegression/common
# make -s -j -C lassoRegression/parallelShootingLasso
# make -s -j -C lassoRegression/parallelShootingLasso2

make -s -j -C setCover/common
make -s -j -C setCover/manis
make -s -j -C setCover/serialDFG
# make -s -j -C setCover/serialGreedy

# make -s -j -C spmv/common
# make -s -j -C spmv/pSPMV
# make -s -j -C spmv/sSPMV

make -s -j -C testData/sequenceData
make -s -j -C testData/sequenceData/data

make -s -j -C testData/graphData
make -s -j -C testData/graphData/data

make -s -j -C testData/geometryData
make -s -j -C testData/geometryData/data
