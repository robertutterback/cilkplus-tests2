#!/bin/bash
set -e

cd pbbs/testData

# Generate graph data
cd graphData/data

# J (MIS, BFS)
make randLocalGraph_J_5_10000
make randLocalGraph_J_5_5000000 # paper evaluation

# E (matching)
make randLocalGraph_E_5_10000
make randLocalGraph_E_5_100000
make randLocalGraph_E_5_1000000
make randLocalGraph_E_5_5000000 # paper evaluation
cd -

# Generate geometric data (delaunaryRefine)

# Need to compile the delaunary program
pushd ../delaunayTriangulation
cd common; make clean; make CILK=1; cd ..
make CILK=1 -C incrementalDelaunay
popd

cd geometryData/data
make 2DinCubeDelaunay_10000
make 2DinCubeDelaunay_100000
make 2DinCubeDelaunay_1000000 # paper evaluation
cd -

# Generate sequence data
cd sequenceData/data
make randomSeq_10M_100K_int # paper evaluation
cd -
